# Pull base image.
FROM maven:3.6.0-jdk-8

COPY ./ back/

RUN cd back && mvn clean install -DskipTests

EXPOSE 8080
EXPOSE 5432

ENTRYPOINT [ "sh", "-c", "java -jar back/target/todolist-0.0.1-SNAPSHOT.jar" ]
