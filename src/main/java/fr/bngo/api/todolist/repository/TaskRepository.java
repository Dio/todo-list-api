package fr.bngo.api.todolist.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import fr.bngo.api.todolist.model.Task;

public interface TaskRepository extends JpaRepository<Task, Long> {
}
