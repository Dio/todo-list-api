package fr.bngo.api.todolist.service;

import java.util.List;
import java.util.Optional;

import fr.bngo.api.todolist.model.Task;

public interface ITaskService {
	
	Task save(Task task);
	List<Task> findAll();
	Optional<Task> find(Long id);
	boolean delete(Long id);
}
