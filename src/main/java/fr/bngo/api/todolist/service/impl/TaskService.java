package fr.bngo.api.todolist.service.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.bngo.api.todolist.model.Task;
import fr.bngo.api.todolist.repository.TaskRepository;
import fr.bngo.api.todolist.service.ITaskService;

@Service
public class TaskService implements ITaskService {

	public static Logger LOGGER = LoggerFactory.getLogger(TaskService.class);
	
	private final TaskRepository taskRepository;

    @Autowired
    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task save(Task task) {
    	LOGGER.info("Begin save");
        try {
        	return taskRepository.save(task);
        } catch (Exception e) {
        	LOGGER.error("Exception occured when trying to save task");
        	throw e;
        }
    }

    public List<Task> findAll() {
    	LOGGER.info("Begin findAll");
        try {
        	return taskRepository.findAll();
        } catch (Exception e) {
        	LOGGER.error("Exception occured when trying to find task");
        	throw e;
        }
    }
    
    public Optional<Task> find(Long id) {
    	LOGGER.info("Begin find");
        try {
        	return taskRepository.findById(id);
        } catch (Exception e) {
        	LOGGER.error("Exception occured when trying to find task");
        	throw e;
        }
    }
    
    public boolean delete(Long id) {
		try {
			taskRepository.deleteById(id);
			LOGGER.info("Deleted Task " + id);
			return true;
		} catch (Exception e) {
			LOGGER.error("Exception occured when trying to remove task");
        	throw e;
		}
	}
	
}
