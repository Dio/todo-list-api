package fr.bngo.api.todolist.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fr.bngo.api.todolist.DTO.TaskDTO;
import fr.bngo.api.todolist.model.Task;
import fr.bngo.api.todolist.service.ITaskService;

@RestController
@RequestMapping("/tasks")
public class TaskController {
    private final ITaskService taskService;

    @Autowired
    public TaskController(ITaskService userService) {
        this.taskService = userService;
    }

    @CrossOrigin(origins = "http://127.0.0.1:4200")
    @GetMapping("/")
    public ResponseEntity<List<Task>> findAll() {
    	return ResponseEntity.ok(taskService.findAll());
    }
    
    @CrossOrigin(origins = "http://127.0.0.1:4200")
    @GetMapping("/{id}")
    public ResponseEntity<Task> find(@PathVariable("id") Long id) {
    	return ResponseEntity.ok(taskService.find(id).get());
    }
    
    @CrossOrigin(origins = "http://127.0.0.1:4200")
    @ResponseBody
    @PutMapping("/save")
    public ResponseEntity<Task> save(@RequestBody TaskDTO taskDTO) {
    	return ResponseEntity.ok(taskService.save(taskDTO.dtoToEntity()));
    }
    
    @CrossOrigin(origins = "http://127.0.0.1:4200")
    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> delete(@PathVariable("id") Long id) {
    	return ResponseEntity.ok(taskService.delete(id));
    }


}
