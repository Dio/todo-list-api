package fr.bngo.api.todolist.DTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import fr.bngo.api.todolist.model.Task;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TaskDTO {

	private Long id;
	private String name;
	private boolean checked;

	public TaskDTO(Task entityTask) {
		this.id = entityTask.getId();
		this.name = entityTask.getName();
		this.checked = entityTask.isChecked();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public Task dtoToEntity() {
		Task task = new Task();
		task.setId(this.getId());
		task.setName(this.getName());
		task.setChecked(this.isChecked());
		return task;
	}
}
