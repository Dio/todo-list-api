# Using Docker

## Build image

docker build -t todo-list-api:dev .

## Run Image (dev)

docker run --name todo-list-api -p 8080:8080 todo-list-api:dev
or as a daemon
docker run -d --name todo-list-api -p 8080:8080 todo-list-api:dev